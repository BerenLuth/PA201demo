﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour {

    public float speedMin = 0.1f;
    public float speedMax = 1;
    private float speed;

    private float rotationSpeed = 4.0f;
    Vector3 averageHeading;
    Vector3 averagePosition;
    float neighbourDistance = 2.5f;

    private bool turning = false;    

	// Use this for initialization
	void Start () {
        speed = Random.Range(speedMin, speedMax);
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(transform.position, GlobalFlock.startPosition) > 25)
            turning = true;
        else
            turning = false;

        if (turning)
        {
            Vector3 direction = GlobalFlock.startPosition - transform.position;
            transform.rotation = Quaternion.Slerp(
                transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        }
        

        if (Random.Range(0, 5) < 1)
            applyRules();

        transform.Translate(0, 0, Time.deltaTime * speed);
	}

    void applyRules()
    {
        GameObject[] gos = GlobalFlock.allFish;

        Vector3 vcenter = GlobalFlock.startPosition;
        Vector3 vavoid = GlobalFlock.startPosition;
        float gSpeed = 0.0f;

        Vector3 goalPos = GlobalFlock.goalPos;

        float dist;

        int groupSize = 0;
        foreach( GameObject go in gos)
        {
            if( go != this.gameObject)
            {
                dist = Vector3.Distance(go.transform.position, this.transform.position);
                if(dist <= neighbourDistance)
                {
                    vcenter += go.transform.position;
                    groupSize++;

                    if (dist < 1.0f)
                    {
                        vavoid = vavoid + (this.transform.position - go.transform.position);
                    }

                    Flock anotherFlock = go.GetComponent<Flock>();
                    gSpeed = gSpeed + anotherFlock.speed;
                }
            }
        }

        if(groupSize > 0)
        {
            vcenter = vcenter / groupSize + (goalPos - this.transform.position);
            speed = gSpeed / groupSize;

            Vector3 direction = (vcenter + vavoid) - transform.position;
            if (direction != GlobalFlock.startPosition)
                transform.rotation = Quaternion.Slerp(
                    transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        }

    }
}
