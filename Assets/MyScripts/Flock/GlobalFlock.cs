﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalFlock : MonoBehaviour {

    public GameObject fishPrefab;

    public static int numberOfFishes = 50;
    public static GameObject[] allFish = new GameObject[numberOfFishes];
    public int tankSize = 10;

    public static Vector3 goalPos = Vector3.zero;
    public static Vector3 startPosition = Vector3.zero;

	// Use this for initialization
	void Start () {

        goalPos = transform.position;
        startPosition = transform.position;

        for (int i = 0; i<numberOfFishes; i++)
        {
            Vector3 position = new Vector3(
                Random.Range(-tankSize, tankSize) + transform.position.x,
                Random.Range(-tankSize, tankSize) + transform.position.y,
                Random.Range(-tankSize, tankSize) + transform.position.z);

            allFish[i] = Instantiate(fishPrefab, position, Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
		if(Random.Range(0, 1000) < 10)
        {
            goalPos = new Vector3(
                Random.Range(-tankSize, tankSize) + transform.position.x,
                Random.Range(-tankSize, tankSize) + transform.position.y,
                Random.Range(-tankSize, tankSize) + transform.position.z);
        }
	}
}
