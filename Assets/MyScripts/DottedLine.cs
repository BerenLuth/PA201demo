﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DottedLine : MonoBehaviour {

    public GameObject end;
    public float offsetX = 0;
    public float offsetY = 10.0f;
    public float offsetZ = 0;
    private Vector3 offset;
    

	// Use this for initialization
	void Start () {
        LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>() as LineRenderer;
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetWidth(0.2f, 0.2f);
        lineRenderer.SetVertexCount(2);
        Vector3 offset = new Vector3(offsetX, offsetY, offsetZ);
}
	
	// Update is called once per frame
	void Update () {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0, transform.position + offset);
        lineRenderer.SetPosition(1, end.transform.position + offset);
    }
}
