﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Digger : MonoBehaviour {
    public TerrainDeformer deformer;
    public Transform deformator;

    public Transform prefab;

    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.F))
        {
            Ray rayCheck = new Ray(deformator.position, deformator.forward);
            Debug.DrawRay(rayCheck.origin, rayCheck.direction * 100);

            RaycastHit hitInfo;
            if (Physics.Raycast(rayCheck, out hitInfo, 100))
            {
                deformer.DestroyTerrain(hitInfo.point, 4);
            }
        }


        if (Input.GetKeyDown(KeyCode.R)) //Check this key
        {
            Instantiate(prefab, deformator.position, Quaternion.identity);
            //var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //cube.transform.position = deformator.position;
        }
    }
}
