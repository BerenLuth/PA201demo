﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CheckIfGrabbed : MonoBehaviour {

    public bool isGrabbed = false;

    // Use this for initialization

    void Start()

    {

        //make sure the object has the VRTK script attached... 

        if (GetComponent<VRTK_InteractableObject>() == null)

        {

            Debug.LogError("Team3_Interactable_Object_Extension is required to be attached to an Object that has the VRTK_InteractableObject script attached to it");

            return;

        }


        //subscribe to the event.  NOTE: the "ObectGrabbed"  this is the procedure to invoke if this objectis grabbed.. 

        GetComponent<VRTK_InteractableObject>().InteractableObjectGrabbed += new InteractableObjectEventHandler(ObjectGrabbed);
        GetComponent<VRTK_InteractableObject>().InteractableObjectUngrabbed += new InteractableObjectEventHandler(ObjectUngrabbed);
    }



    //this object has been grabbed.. so do what ever is in the code.. 

    private void ObjectGrabbed(object sender, InteractableObjectEventArgs e)

    {
        isGrabbed = true;
        Debug.Log("Im Grabbed");

    }

    private void ObjectUngrabbed(object sender, InteractableObjectEventArgs e)

    {
        isGrabbed = false;
        Debug.Log("Im Unrabbed");

    }

}
