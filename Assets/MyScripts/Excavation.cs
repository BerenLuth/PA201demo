﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Excavation : MonoBehaviour
{
    public Terrain myTerrain;
    public GameObject playerObject;
    int xResolution;
    int zResolution;
    Vector3 offset; //position of the terrain
    //float[,] heights;
    float[,] heights;
    float[,] heightsBackup;

    const float EXCAVATION_DELTA = 0.0001f;
    const float MAX_DISTANCE = 50;   //Change it for test, but the ideal value is ~5

    void Start()
    {
        //Resolution should be around 500
        xResolution = myTerrain.terrainData.alphamapWidth;     
        zResolution = myTerrain.terrainData.alphamapHeight;
        offset = myTerrain.transform.position;
        heights = Transpose(myTerrain.terrainData.GetHeights(0, 0, xResolution, zResolution));
        heightsBackup = myTerrain.terrainData.GetHeights(0, 0, xResolution, zResolution);

        //var playerObject = GameObject.Find("Player");
    }

    private void OnDestroy(){
        myTerrain.terrainData.SetHeights(0, 0, heightsBackup);
    }

    void Update()
    {
        RaycastHit hit;
        if (!Camera.main)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        //Debug.DrawRay(transform.position, ray.direction, Color.green);

        Vector3 playerPos = playerObject.transform.position;

        if (Input.GetMouseButton(0)){
            if (Physics.Raycast(ray, out hit) && Vector3.Distance(playerPos, hit.point) < MAX_DISTANCE)
            {
                lowerTerrain(hit.point);
            }
        }
        /*if (Input.GetMouseButton(1)){
            if (Physics.Raycast(ray, out hit) && Vector3.Distance(playerPos, hit.point) < MAX_DISTANCE)
            {
                raiseTerrain(hit.point);
            }
        }*/
    }

    public void lowerTerrain(Vector3 point){
        modifyTerrain(point, -1*EXCAVATION_DELTA);
    }

    private void raiseTerrain(Vector3 point){
        modifyTerrain(point, EXCAVATION_DELTA);
    }

    private void modifyTerrain(Vector3 point, float delta)
    {
        //print(xResolution + ", " + zResolution);
        int terX = (int)(((point.x / myTerrain.terrainData.size.x) * xResolution));
        int terZ = (int)(((point.z / myTerrain.terrainData.size.z) * zResolution));
        float prev = heights[terX, terZ];
        float y = prev;
        y += delta;
        //print(y);
        if (y <= 0f)
            y = 0f;
        float[,] height = new float[1, 1];
        height[0, 0] = y;
        heights[terX, terZ] = y;

        myTerrain.terrainData.SetHeights(terX, terZ, height);

        /*var terrainLocalPos = point - myTerrain.transform.position;
        var normalizePos = new Vector2(Mathf.InverseLerp(0.0f, myTerrain.terrainData.size.x, terrainLocalPos.x),
                                   Mathf.InverseLerp(0.0f, myTerrain.terrainData.size.z, terrainLocalPos.z));

        float y = heights[(int) normalizePos.x, (int) normalizePos.y];
        y += delta;
        if (y <= 0) y = 0;
        float[,] height = new float[1, 1];
        height[0, 0] = y;
        heights[(int) normalizePos.x, (int) normalizePos.y] = y;

        myTerrain.terrainData.SetHeights((int) normalizePos.x, (int) normalizePos.y, height);*/

    }

    public float[,] Transpose(float[,] matrix)
    {
        int w = matrix.GetLength(0);
        int h = matrix.GetLength(1);

        float[,] result = new float[h, w];

        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                result[j, i] = matrix[i, j];
            }
        }

        return result;
    }
}
